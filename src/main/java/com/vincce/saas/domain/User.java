package com.vincce.saas.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created By BaoNing On 2019/3/18
 */
@Data
@Document(collection = "t_user")
public class User {

    @Id
    private String id;

    @Field(value = "username")
    private String username;

    @Field(value = "password")
    private String password;

    @Field(value = "phone")
    private String phone;

}
