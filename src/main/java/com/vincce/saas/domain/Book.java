package com.vincce.saas.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Created By BaoNing On 2019/3/21
 */
@Data
@Document(collection = "t_book")
public class Book {

    @Id
    private String id;

    @Field(value = "name")
    private String name;

    @Field(value = "price")
    private Integer price;

    @Field(value = "type")
    private String type;


}
