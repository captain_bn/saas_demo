package com.vincce.saas.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created By BaoNing On 2019/3/1
 */
@Data
@Document(collection = "t_card")
public class Card {

    @Id
    private String id;

    /**
     *姓名
     */
    @NotNull
    @Field(value = "name")
    private String name;

    /**
     * 年龄
     */
    @Field(value = "age")
    private Integer age;

    /**
     * 头像
     */
    @NotNull
    @Field(value = "head_pic")
    private String headPic;

    /**
     * 手机号
     */
    @NotNull
    @Field(value = "phone")
    private String phone;

    /**
     * 微信号
     */
    @Field(value = "weChat")
    private String weChat;

    /**
     * 职业
     */
    @Field(value = "career")
    private String career;

    /**
     * 地址
     */
    @Field(value = "location")
    private String location;

    /**
     * 公司
     */
    @Field(value = "company")
    private String company;

    /**
     * 创建时间
     */
    @Field(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Field(value = "update_time")
    private Date updateTime;

    /**
     * 状态值
     */
    @Field(value = "status")
    private Integer status;

    /**
     * 用户id
     */
    @Field(value = "user_id")
    private String userId;

}
