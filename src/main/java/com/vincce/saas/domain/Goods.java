package com.vincce.saas.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

/**
 * @Author BaoNing 2019/3/31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "i_goods",type = "t_goods")
public class Goods implements Serializable {

    private String id;

    private String name;

    private String description;

}
