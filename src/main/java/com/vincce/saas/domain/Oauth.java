package com.vincce.saas.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * Created By BaoNing On 2019/3/18
 */
@Data
public class Oauth {

    @JSONField(name = "access_token")
    public String accessToken;

    @JSONField(name = "token_type")
    public String tokenType;

    @JSONField(name = "expires_in")
    public Integer expiresIn;

    @JSONField(name = "refresh_token")
    public String refreshToken;

}
