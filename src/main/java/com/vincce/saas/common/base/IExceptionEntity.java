package com.vincce.saas.common.base;

/**
 * Created By BaoNing On 2019/3/18
 */
public interface IExceptionEntity {

    String getCode();

    String getContent();
}
