package com.vincce.saas.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created By BaoNing On 2019/3/18
 */
@Data
@Component
@ConfigurationProperties("oauth")
public class OauthProperties {

    public Integer accessTokenLength;

    public Integer refreshTokenLength;

    public Integer expiresIn;

    public Integer refreshTokenExpiresIn;

    public Map<String, String> clients;
}
