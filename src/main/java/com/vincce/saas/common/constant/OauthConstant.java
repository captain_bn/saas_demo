package com.vincce.saas.common.constant;

/**
 * Created By BaoNing On 2019/3/18
 */
public class OauthConstant {

    public static final class CLIENT_ID {
        public static final String ADMIN = "admin";
    }

    public static final class TOKEN_PREFIX {
        public static final String ADMIN = "b";
    }

    public static final class TOKEN_TYPE {
        public static final String BEARER = "bearer";
    }

    public static final class GRANT_TYPE {
        public static final String PASSWORD = "password";
        public static final String REFRESH_TOKEN = "refresh_token";
    }

}
