package com.vincce.saas.common.constant;

/**
 * Created By BaoNing On 2019/3/18
 */
public class RedisConstant {

    public static final class KEY_PREFIX {
        //OAUTH
        public static final String ACCESS_TOKEN = "AT_";
        public static final String REFRESH_TOKEN = "RT_";
        public static final String REVERSE_ACCESS_TOKEN = "RAT_";
        public static final String REVERSE_REFRESH_TOKEN = "RRT_";

        //ADMIN
        public static final String ADMIN = "A_";
        public static final String ADMIN_REVERSE = "AR_";
    }

    public static final class EXPIRE{
        //USER
        public static final int USER = 2*60*60;
    }
}
