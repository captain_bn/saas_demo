package com.vincce.saas.common.constant;

/**
 * Created By BaoNing On 2019/3/18
 */
public class ResultConstant {

    //result
    public static final class RESULT {
        public static final String SUCCESS = "添加成功!";
        public static final String FAIL = "添加失败!";
    }

    //login
    public static final class LOGIN {
        public static final String SUCCESS = "登录成功!";
        public static final String FAIL = "登录失败!";
    }

}
