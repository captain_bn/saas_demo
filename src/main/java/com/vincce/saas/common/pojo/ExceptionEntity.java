package com.vincce.saas.common.pojo;

import com.vincce.saas.common.base.IExceptionEntity;

import java.io.Serializable;

/**
 * Created By BaoNing On 2019/3/18
 */
public enum ExceptionEntity implements Serializable, IExceptionEntity {

    /* http code description
200: GET请求成功，及DELETE或PATCH同步请求完成，或者PUT同步更新一个已存在的资源
201: POST 同步请求完成，或者PUT同步创建一个新的资源
202: POST，PUT，DELETE，或PATCH请求接收，将被异步处理
206: GET 请求成功，但是只返回一部分
304: GET 请求成功，但是内容没有更新
400: GET 请求失败 ，服务器不理解请求的语法

使用身份认证（authentication）和授权（authorization）错误码时需要注意：
401 Unauthorized: 用户未认证，请求失败
403 Forbidden: 用户无权限访问该资源，请求失败

当用户请求错误时，提供合适的状态码可以提供额外的信息：
405 Method Not Allowed: 该URL不支持该访问方式
422 Unprocessable Entity: 请求被服务器正确解析，但是包含无效字段
429 Too Many Requests: 因为访问频繁，你已经被限制访问，稍后重试
500 Internal Server Error: 服务器错误，确认状态并报告问题
*/
    /* error code rule;
        系统(还未涉及业务):"SY00001"
     */
    //system errors
    SYS_UNKOWN_ERROR("SY99999",  "系统未知异常", 400),
    SYS_PARAMS_ERROR("SY00001",  "请求参数异常", 400),
    SYS_UPDATE_ERROR("SY00002",  "系统更新异常", 500),
    SYS_INSERT_ERROR("SY00003",  "系统插入异常", 500),
    SYS_PARAMS_NOT_COMPLETE_ERROR("SY00004",  "参数不完整", 500),
    SYS_PARAMS_NO_ID_ERROR("SY00005",  "缺少参数id", 500),
    SYS_DETAIL_EMPTY_ERROR("SY00006",  "查询详情不存在", 404),
    SYS_FILE_EMPTY_ERROR("SY00007",  "缺少文件", 404),
    SYS_PARAMS_LACK_ERROR("SY00008",  "缺少请求参数", 400),

    ;

    ExceptionEntity(String code, String content, int httpStatus) {
        this.code = code;
        this.content = content;
        this.httpStatus = httpStatus;
    }

    private String code;
    private String content;
    private int httpStatus;

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

}
