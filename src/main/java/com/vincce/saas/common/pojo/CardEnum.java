package com.vincce.saas.common.pojo;

/**
 * Created By BaoNing On 2019/3/11
 */
public enum CardEnum {

    SUCCESS(200,"成功"),FAIL(500,"失败");

    private Integer status;
    private String description;

    private CardEnum(Integer status, String description){
        this.status = status;
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

}
