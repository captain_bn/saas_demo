package com.vincce.saas.common.pojo;

import lombok.Data;

/**
 * Created By BaoNing On 2019/3/19
 */
@Data
public class Login {

    private String username;

    private String password;
}
