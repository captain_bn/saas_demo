package com.vincce.saas.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import com.mongodb.MongoClientURI;

/**
  Created By BaoNing On 2019年2月28日
*/
@Configuration
public class MongoDBConfig {
	
	@Value("${spring.data.mongodb.uri}")
	private String uri;

	@Value("${spring.data.mongodb.database}")
	private String database;
	
	@Bean
	public MongoDbFactory mongoDbFactory() throws Exception {
		String uriStr= uri + "/" + database;
		MongoClientURI mongoClientURI = new MongoClientURI(uriStr);
        MongoDbFactory mongoDbFactory= new SimpleMongoDbFactory(mongoClientURI);
        return mongoDbFactory;
	}
	

}
