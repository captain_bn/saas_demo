package com.vincce.saas.config;

import lombok.Data;

/**
 * Created By BaoNing On 2019/3/7
 */
//@ConfigurationProperties("kafka.topic")
@Data
public class KafkaProperties {

    private String groupId;

    private String[] topicName;
}
