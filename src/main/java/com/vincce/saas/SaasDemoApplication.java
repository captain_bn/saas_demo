package com.vincce.saas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SaasDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasDemoApplication.class, args);
    }

}
