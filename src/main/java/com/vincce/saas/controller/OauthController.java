package com.vincce.saas.controller;

import com.vincce.saas.common.constant.RedisConstant;
import com.vincce.saas.common.properties.OauthProperties;
import com.vincce.saas.domain.Oauth;
import com.vincce.saas.service.OauthService;
import com.vincce.saas.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.AccessDeniedException;
import java.util.concurrent.TimeUnit;

/**
 * Created By BaoNing On 2019/3/18
 */
@RestController
@RequestMapping("/api/oauth")
public class OauthController {

    @Autowired
    private OauthService oauthService;
    @Autowired
    private OauthProperties oauthProperties;
    @Autowired
    private RedisTemplate<String,String> oauthRedisTemplate;


    /**
     * 验证客户端
     * @param clientId
     * @param clientSecret
     * @return
     */
    private boolean checkClient(String clientId, String clientSecret){
        return StringUtils.isNotEmpty(clientId)
                && StringUtils.isNotEmpty(clientSecret)
                && clientSecret.equals(oauthProperties.clients.get(clientId));
    }

//    private Oauth tokenByPassword(Login login, String clientId, HttpServletRequest req) throws AccessDeniedException{
//        if(StringUtils.isEmpty(login.getUsername()) || StringUtils.isEmpty(login.getPassword())) {
//            throw new AccessDeniedException("oauth");
//        }
//        String id = oauthService.fetchByPassword(login.getUsername(), login.getPassword(), clientId, req);
//        if(id != null) {
//            return genAndCache(id, clientId);
//        }
//        //  throw new AccessDeniedException("oauth");
//        Oauth oauth = new Oauth();
//        oauth.setError("用户名或密码错误");
//        return oauth;
//    }

    private Oauth tokenByRefresh(String refreshToken, String clientId) throws AccessDeniedException{
        if (StringUtils.isEmpty(refreshToken)) {
            throw new AccessDeniedException("oauth");
        }

        String id = oauthRedisTemplate.opsForValue().get(RedisConstant.KEY_PREFIX.REFRESH_TOKEN + refreshToken);
        if (!StringUtils.isEmpty(id)) {
            return genAndCache(id, clientId);
        }

        throw new AccessDeniedException("oauth");
    }


    private Oauth genAndCache(String id, String clientId) {
        Oauth oauth = oauthService.genTokens(clientId);

        //save tokens to redis
        oauthRedisTemplate.opsForValue().set(RedisConstant.KEY_PREFIX.ACCESS_TOKEN  + oauth.accessToken,id);
        oauthRedisTemplate.opsForValue().set(RedisConstant.KEY_PREFIX.REFRESH_TOKEN + oauth.refreshToken, id);
        oauthRedisTemplate.expire(RedisConstant.KEY_PREFIX.ACCESS_TOKEN + oauth.accessToken, oauthProperties.expiresIn, TimeUnit.SECONDS);
        oauthRedisTemplate.expire(RedisConstant.KEY_PREFIX.REFRESH_TOKEN + oauth.refreshToken,oauthProperties.refreshTokenExpiresIn,TimeUnit.SECONDS);


        //remove reverse tokens
        String reverseRefreshToken = oauthRedisTemplate.opsForValue().get(RedisConstant.KEY_PREFIX.REVERSE_REFRESH_TOKEN + id);
        if (!StringUtils.isEmpty(reverseRefreshToken)) {
            oauthRedisTemplate.delete(RedisConstant.KEY_PREFIX.REVERSE_REFRESH_TOKEN +  id);
            oauthRedisTemplate.delete(RedisConstant.KEY_PREFIX.REFRESH_TOKEN + reverseRefreshToken);
        }
        String reverseAccessToken = oauthRedisTemplate.opsForValue().get(RedisConstant.KEY_PREFIX.REVERSE_ACCESS_TOKEN + id);
        if (!StringUtils.isEmpty(reverseAccessToken)) {
            oauthRedisTemplate.delete(RedisConstant.KEY_PREFIX.REVERSE_ACCESS_TOKEN + id);
            oauthRedisTemplate.delete(RedisConstant.KEY_PREFIX.ACCESS_TOKEN + reverseAccessToken);
        }

        //add new reverse tokens
        oauthRedisTemplate.opsForValue().set(RedisConstant.KEY_PREFIX.REVERSE_REFRESH_TOKEN + id, oauth.refreshToken);
        oauthRedisTemplate.opsForValue().set(RedisConstant.KEY_PREFIX.REVERSE_ACCESS_TOKEN + id, oauth.accessToken);
        oauthRedisTemplate.expire(RedisConstant.KEY_PREFIX.REVERSE_ACCESS_TOKEN + id, oauthProperties.expiresIn, TimeUnit.SECONDS);
        oauthRedisTemplate.expire(RedisConstant.KEY_PREFIX.REVERSE_REFRESH_TOKEN + id, oauthProperties.refreshTokenExpiresIn, TimeUnit.SECONDS);

        return oauth;
    }



}
