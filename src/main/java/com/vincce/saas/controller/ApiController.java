package com.vincce.saas.controller;

import com.vincce.saas.domain.Book;
import com.vincce.saas.service.ApiService;
import com.vincce.saas.vo.ApiVo;
import com.vincce.saas.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/21
 */
@RestController
@RequestMapping("/api/book")
public class ApiController {

    @Autowired
    private ApiService apiService;

    /**
     * save
     * @param bookVo
     */
    @PostMapping("/save")
    public void save(@RequestBody BookVo bookVo){
        apiService.save(bookVo);
    }

    /**
     * insertAll
     * @param bookList
     */
    @PostMapping("/insertAll")
    public void insertAll(@RequestBody List<Book> bookList){
        apiService.insertAll(bookList);
    }

    /**
     * findById
     * @param id
     * @return
     */
    @GetMapping("/findById")
    public Book findById(@RequestParam("id") String id){
        return apiService.findById(id);
    }

    /**
     * findAll
     * @return
     */
    @GetMapping("/findAll")
    public List<Book> findAll(){
        return apiService.findAll();
    }

    /**
     * upsert
     * @param book
     */
    @PostMapping("/upsert")
    public void upsert(@RequestBody Book book){
        apiService.upsert(book);
    }

    /**
     * findAndModify
     * @param book
     * @return
     */
    @PostMapping("/findAndModify")
    public Boolean findAndModify(@RequestBody Book book){
        return apiService.findAndModify(book);
    }

    /**
     * findAndRemove
     * @param id
     * @return
     */
    @GetMapping("/findAndRemove")
    public Book findAndRemove(String id){
        return apiService.findAndRemove(id);
    }

    /**
     * count
     * @param apiVo
     * @return
     */
    @PostMapping("/count")
    public Long count(@RequestBody ApiVo apiVo){
        String key = apiVo.getKey();
        Object value = apiVo.getValue();
        return apiService.count(key, value);
    }

    /**
     * exists
     * @param id
     * @return
     */
    @GetMapping("/exists")
    public Boolean exists(String id){
        return apiService.exists(id);
    }




}
