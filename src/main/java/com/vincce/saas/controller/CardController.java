package com.vincce.saas.controller;

import com.vincce.saas.common.base.APage;
import com.vincce.saas.common.base.ResultEntity;
import com.vincce.saas.common.pojo.ExceptionEntity;
import com.vincce.saas.domain.Card;
import com.vincce.saas.service.CardService;
import com.vincce.saas.vo.CardVo;
import com.vincce.saas.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/1
 */
@RestController
@RequestMapping("/api/card")
public class CardController {

    @Autowired
    private CardService cardService;


    /**
     * updateCard
     * @param cardVo
     * @return
     */
    @PostMapping("/updateCard")
    public Card updateCard(@RequestBody CardVo cardVo){
        return cardService.updateCard(cardVo);
    }


    /**
     * addCard
     * @param cardVo
     * @return
     */
    @PostMapping("/add")
    public boolean addCard(@RequestBody CardVo cardVo){
        return cardService.addCard(cardVo);
    }

    /**
     * getCard
     * @param name
     * @return
     */
    @GetMapping("/get")
    public Card getCard(@RequestParam String name){
        //redis缓存穿透问题
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                cardService.getCard(name);
//            }
//        };
//        ExecutorService executorService = Executors.newFixedThreadPool(25);
//        for (int i =0; i < 10000; i++){
//            executorService.submit(runnable);
//        }
        return cardService.getCard(name);
    }

    /**
     * getAllCard
     * @return
     */
    @GetMapping("/all")
    public List<Card> getAllCard(){
        return cardService.getAllCard();
    }

    /**
     *
     * @param name
     * @param phone
     * @return
     */
    @PostMapping("/update")
    public boolean updatePhoneByName(@RequestParam String name, @RequestParam String phone){
        cardService.updatePhoneByName(name, phone);
        return true;
    }

    /**
     * deleteCard
     * @param name
     * @return
     */
    @DeleteMapping("/delete")
    public boolean deleteCard(@RequestParam String name){
        return cardService.deleteCard(name);
    }

    /**
     * insert
     * @param cardVo
     */
    @PostMapping("/insert")
    public void insert(@RequestBody CardVo cardVo){
        cardService.insert(cardVo);
    }

    /**
     * insert list
     * @param cardVo
     */
    @PostMapping("/insertList")
    public void insertList(@RequestBody CardVo cardVo){
        cardService.insertList(cardVo);
    }

    /**
     * find and update
     * @param cardVo
     */
    @PostMapping("/findAndUpdate")
    public void findAndUpdate(@RequestBody CardVo cardVo){
        cardService.findAndUpdate(cardVo);
    }

    /**
     * query by specify operators
     * @return
     */
    @GetMapping("/queryByOperators")
    public List<Card> queryByOperators(@RequestParam Integer paramter){
        return cardService.queryByOperators(paramter);
    }

    /**
     * query by pageable
     * @param pageVo
     * @return
     */
    @PostMapping("/queryByPageable")
    public APage<Card> queryByPageable(@RequestBody PageVo pageVo){
        return cardService.queryByPageable(pageVo);
    }

    /**
     * 通过userId查询
     * @param userId
     * @return
     */
    @GetMapping("/queryByUserId")
    public ResultEntity queryCardListByUserId(@RequestParam("userId") String userId){
        if (StringUtils.isEmpty(userId)){
            return new ResultEntity(ExceptionEntity.SYS_PARAMS_LACK_ERROR);
        }
        return new ResultEntity(cardService.queryCardListByUserId(userId));
    }

}
