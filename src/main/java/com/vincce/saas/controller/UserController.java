package com.vincce.saas.controller;

import com.vincce.saas.common.constant.ResultConstant;
import com.vincce.saas.service.UserService;
import com.vincce.saas.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created By BaoNing On 2019/3/19
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * add user
     * @param userVo
     * @return
     */
    @PostMapping("/add")
    public String addUser(@RequestBody UserVo userVo){
        boolean result = userService.addUser(userVo);
        if (result){
            return ResultConstant.RESULT.SUCCESS;
        }else {
            return ResultConstant.RESULT.FAIL;
        }
    }

    /**
     * delete by id
     * @param id
     */
    @PostMapping("/deleteById")
    public void deleteById(@RequestParam("id") String id){
        userService.deleteById(id);
    }



}
