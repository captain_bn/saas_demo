package com.vincce.saas.controller;

import com.vincce.saas.common.constant.ResultConstant;
import com.vincce.saas.domain.User;
import com.vincce.saas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created By BaoNing On 2019/3/19
 */
@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private UserService userService;

    /**
     * login
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/login")
    public String login(@RequestParam("username")String username,
                        @RequestParam("password")String password){
        User user = userService.login(username, password);
        if (user != null){
            return ResultConstant.LOGIN.SUCCESS;
        }else {
            return ResultConstant.LOGIN.FAIL;
        }
    }
}
