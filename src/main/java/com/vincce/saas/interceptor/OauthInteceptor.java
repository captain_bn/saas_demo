package com.vincce.saas.interceptor;

import com.vincce.saas.service.OauthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.file.AccessDeniedException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created By BaoNing On 2019/3/18
 */
@Component
public class  OauthInteceptor extends HandlerInterceptorAdapter {

    private static Pattern pattern = Pattern.compile("Bearer (\\S+)");

    @Autowired
    private OauthService oauthService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authorization = request.getHeader("Authorization");
        if (StringUtils.isEmpty(authorization)){
            throw new AccessDeniedException("oauth");
        }
        Matcher matcher = pattern.matcher(authorization);
        if (!matcher.matches()){
            throw new AccessDeniedException("oauth");
        }
        String accessToken = matcher.group(1);
        oauthService.setAttribute(request,accessToken);
        return super.preHandle(request, response, handler);
    }

}
