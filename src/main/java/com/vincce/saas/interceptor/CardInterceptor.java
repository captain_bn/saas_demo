package com.vincce.saas.interceptor;

import com.vincce.saas.common.constant.AttributeConstant;
import com.vincce.saas.exception.ForbiddenException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created By BaoNing On 2019/3/18
 */
@Component
public class CardInterceptor extends HandlerInterceptorAdapter{

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getAttribute(AttributeConstant.ADMIN) == null){
            throw new ForbiddenException();
        }
        return super.preHandle(request, response, handler);
    }

}
