package com.vincce.saas.exception;

/**
 * Created By BaoNing On 2019/3/19
 */
public class TipException extends RuntimeException {

    private static final long serialVersionUID = -6417273878717422305L;

    public TipException() {
    }

    public TipException(String message) {
        super(message);
    }

    public TipException(String message, Throwable cause) {
        super(message, cause);
    }

    public TipException(Throwable cause) {
        super(cause);
    }

}
