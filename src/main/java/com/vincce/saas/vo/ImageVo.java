package com.vincce.saas.vo;

import com.vincce.saas.common.pojo.ImageFile;
import lombok.Data;

/**
 * Created By BaoNing On 2019/1/23
 */
@Data
public class ImageVo extends ImageFile {

}
