package com.vincce.saas.vo;

import com.vincce.saas.domain.Card;
import lombok.Data;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/1
 */
@Data
public class CardVo extends Card {

    List<Card> cardList;

}
