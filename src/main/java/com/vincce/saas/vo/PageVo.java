package com.vincce.saas.vo;

import lombok.Data;

/**
 * Created By BaoNing On 2019/3/14
 */
@Data
public class PageVo {

    private Integer pageIndex;

    private Integer pageSize;

    private String queryField;

}
