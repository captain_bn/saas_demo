package com.vincce.saas.vo;

import com.vincce.saas.domain.Card;
import com.vincce.saas.domain.User;
import lombok.Data;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/19
 */
@Data
public class UserVo extends User{

    public List<Card> cardList;
}
