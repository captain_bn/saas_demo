package com.vincce.saas.vo;

import com.vincce.saas.domain.Book;
import lombok.Data;

/**
 * Created By BaoNing On 2019/3/22
 */
@Data
public class ApiVo extends Book{

    private String key;

    private Object value;



}
