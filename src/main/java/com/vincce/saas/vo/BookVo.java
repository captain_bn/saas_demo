package com.vincce.saas.vo;

import com.vincce.saas.domain.Book;
import com.vincce.saas.domain.Card;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/22
 */
@Data
public class BookVo extends Book{

    @Field(value = "card_list")
    private List<Card> cardList;


}
