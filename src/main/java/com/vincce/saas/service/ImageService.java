package com.vincce.saas.service;

import com.vincce.saas.common.pojo.Image;
import com.vincce.saas.util.ImageUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
  Created By BaoNing On 2019年1月22日
*/
@Service
public class ImageService {

    /**
     * 新增图片
     *
     * @param file
     * @return
     */
    @Transactional
    public Image insertImage(MultipartFile file) {
        //图片上传七牛云后，并返回
        Image image = ImageUtil.uploadImage(file, file.getOriginalFilename());
        return image;
    }


}
