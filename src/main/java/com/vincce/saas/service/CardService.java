package com.vincce.saas.service;

import com.vincce.saas.common.base.APage;
import com.vincce.saas.common.constant.CardRedisConstant;
import com.vincce.saas.common.pojo.CardEnum;
import com.vincce.saas.dao.CardDao;
import com.vincce.saas.domain.Card;
import com.vincce.saas.vo.CardVo;
import com.vincce.saas.vo.PageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created By BaoNing On 2019/3/1
 */
@Service
@Slf4j
public class CardService {

    @Autowired
    private CardDao cardDao;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * updateCard
     * @param cardVo
     * @return
     */
    public Card updateCard(CardVo cardVo){
        Card card = cardDao.udpateCard(cardVo);
        return card;
    }


    /**
     * add
     * @return
     */
    @Transactional
    public boolean addCard(CardVo cardVo){
        Card card = new Card();
        //属性赋值
        BeanUtils.copyProperties(cardVo, card);
        card.setCreateTime(new Date());
        card.setStatus(CardEnum.SUCCESS.getStatus());
        try {
            cardDao.addCard(card);
            log.info("add success!");
        }catch (Exception e){
            e.printStackTrace();
            log.debug("add failure!");
            return false;
        }
        return true;
    }

    /**
     * get
     * @param name
     * @return
     */
    public Card getCard(String name){
//        //从redis中取值
//        Card card = (Card)redisTemplate.opsForValue().get(CardRedisConstant.KEY_PREFIX.Card_Moudle + name);
//        if(null == card){
//            //从数据库取值
//            System.out.println("查询数据库......");
//            card = cardDao.getCard(name);
//            //缓存到redis中
//            redisTemplate.opsForValue().set(CardRedisConstant.KEY_PREFIX.Card_Moudle + card.getName(),card);
//        }else {
//            System.out.println("查询缓存......");
//        }
//        return card;


        //从redis中取值
        //解决数据库缓存穿透问题
        Card card = (Card)redisTemplate.opsForValue().get(CardRedisConstant.KEY_PREFIX.Card_Moudle + name);
        //双重检测锁
        if(null == card){
            synchronized (this){
                card = (Card)redisTemplate.opsForValue().get(CardRedisConstant.KEY_PREFIX.Card_Moudle + name);
                if (null == card){
                    System.out.println("查询数据库......");
                    card = cardDao.getCard(name);
                    redisTemplate.opsForValue().set(CardRedisConstant.KEY_PREFIX.Card_Moudle + card.getName(),card);
                }else {
                    System.out.println("查询缓存......");
                }
            }
        }else {
            System.out.println("查询缓存......");
        }
        return card;
    }

    /**
     * all
     * @return
     */
    public List<Card> getAllCard(){
        return cardDao.getAllCard();
    }

    /**
     * update
     * @param name phone
     * @return
     */
    public boolean updatePhoneByName(String name, String phone){
        cardDao.updatePhoneByName(name,phone);
        return true;
    }

    /**
     * delete
     * @param name
     * @return
     */
    public boolean deleteCard(String name){
        try {
            //清除缓存
            redisTemplate.delete(CardRedisConstant.KEY_PREFIX.Card_Moudle + name);
            //清除数据库
            cardDao.deleteCard(name);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 测试
     * @param cardVo
     */
    public void insert(CardVo cardVo) {
        Card card = new Card();
        BeanUtils.copyProperties(cardVo, card);
        card.setCreateTime(new Date());
        card.setStatus(CardEnum.SUCCESS.getStatus());
        cardDao.insert(card);
    }

    /**
     * 同时插入多个数据
     * @param cardVo
     */
    public void insertList(CardVo cardVo){
        List<Card> cardList = cardVo.getCardList();
        cardDao.insertList(cardList);
    }

    /**
     * 查询并修改
     * @param cardVo
     */
    public void  findAndUpdate(CardVo cardVo){
        cardDao.findAndUpdate(cardVo);
    }

    /**
     * 操作符查询
     * @return
     */
    public List<Card> queryByOperators(Integer paramter){
        return cardDao.queryByOperators(paramter);
    }

    /**
     * 分页查询
     * @param pageVo
     * @return
     */
    public APage<Card> queryByPageable(PageVo pageVo){
        return cardDao.queryByPageable(pageVo);
    }

    public APage<Card> queryCardListByUserId(String userId){
        return cardDao.queryCardListByUserId(userId);
    }



}
