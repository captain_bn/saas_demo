package com.vincce.saas.service;

import com.vincce.saas.dao.ApiDao;
import com.vincce.saas.domain.Book;
import com.vincce.saas.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/21
 */
@Service
public class ApiService {

    @Autowired
    private ApiDao apiDao;

    /**
     * save
     * @param bookVo
     */
    public void save(BookVo bookVo){
        apiDao.save(bookVo);
    }

    /**
     * insertAll
     * @param bookList
     */
    public void insertAll(List<Book> bookList){
        apiDao.insertAll(bookList);
    }

    /**
     * findById
     * @param id
     * @return
     */
    public Book findById(String id){
        return apiDao.findById(id);
    }

    /**
     * findAll
     * @return
     */
    public List<Book> findAll(){
        List<Book> bookList = apiDao.findAll();
        bookList.forEach(book -> {
            book.setName("book_1");
            book.setPrice(book.getPrice()+1000);
        });
        return bookList;
    }

    /**
     * upsert
     * @param book
     */
    public void upsert(Book book){
        apiDao.upsert(book);
    }

    /**
     * findAndModify
     * @param book
     * @return
     */
    public Boolean findAndModify(Book book){
        return apiDao.findAndModify(book);
    }

    /**
     * findAndRemove
     * @param id
     * @return
     */
    public Book findAndRemove(String id){
        return apiDao.findAndRemove(id);
    }

    /**
     * count
     * @param key
     * @param value
     * @return
     */
    public Long count(String key, Object value){
        return apiDao.count(key,value);
    }

    /**
     * exists
     * @param id
     * @return
     */
    public Boolean exists(String id){
        return apiDao.exists(id);
    }



}
