package com.vincce.saas.service;

import com.vincce.saas.dao.CardDao;
import com.vincce.saas.dao.UserDao;
import com.vincce.saas.domain.User;
import com.vincce.saas.exception.TipException;
import com.vincce.saas.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created By BaoNing On 2019/3/19
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private CardDao cardDao;

    /**
     * get userId
     * @param username
     * @param password
     * @return
     */
    public String getUserId(String username, String password){
        User user = userDao.checkUser(username, password);
        String userId = user.getId();
        return userId;
    }

    /**
     * login
     * @param username
     * @param password
     * @return
     */
    public User login(String username,String password){
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            throw new TipException("用户或密码不能为空!");
        }
        User user = userDao.checkUser(username, password);
        if (user == null){
            throw new TipException("用户名或密码错误!");
        }
        return user;
    }

    /**
     * add user
     * @param userVo
     * @return
     */
    public boolean addUser(UserVo userVo){
        return userDao.addUser(userVo);
    }

    /**
     * delete by id
     * @param id
     */
    public void deleteById(String id){
        userDao.deleteById(id);
    }






}
