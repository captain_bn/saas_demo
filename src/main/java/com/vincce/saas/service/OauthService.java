package com.vincce.saas.service;

import com.vincce.saas.common.constant.AttributeConstant;
import com.vincce.saas.common.constant.OauthConstant;
import com.vincce.saas.common.constant.RedisConstant;
import com.vincce.saas.common.properties.OauthProperties;
import com.vincce.saas.domain.Oauth;
import com.vincce.saas.util.RandomStringUtils;
import com.vincce.saas.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.AccessDeniedException;

/**
 * Created By BaoNing On 2019/3/18
 */
@Service
public class OauthService {

    @Autowired
    private OauthProperties oauthProperties;
    @Autowired
    private RedisTemplate<String,String> oauthRedisTemplate;
    @Autowired
    private UserService userService;

    /**
     * 获取token
     * @param clientId
     * @return
     */
    public Oauth genTokens(String clientId){
        String prefix = OauthConstant.TOKEN_PREFIX.ADMIN;
        Oauth oauth = new Oauth();
        oauth.accessToken = prefix + RandomStringUtils.random(oauthProperties.accessTokenLength);
        oauth.tokenType = OauthConstant.TOKEN_TYPE.BEARER;
        oauth.refreshToken = prefix + RandomStringUtils.random(oauthProperties.refreshTokenLength);
        oauth.expiresIn = oauthProperties.expiresIn;
        return oauth;
    }

    /**
     * 设置参数
     * @param request
     * @param accessToken
     * @throws AccessDeniedException
     */
    public void setAttribute(HttpServletRequest request, String accessToken) throws AccessDeniedException{
        String userId = oauthRedisTemplate.opsForValue().get(RedisConstant.KEY_PREFIX.ACCESS_TOKEN + accessToken);
        if(null ==userId){
            throw new AccessDeniedException("oauth");
        }
        if (accessToken.startsWith(OauthConstant.TOKEN_PREFIX.ADMIN)){
            request.setAttribute(AttributeConstant.ADMIN,userId);
        }else {
            throw new AccessDeniedException("ouath");
        }
    }

    /**
     * 获取userId
     * @param username
     * @param password
     * @param clientId
     * @return
     */
    public String fetchByPassword(String username, String password, String clientId) {
        if(clientId.equals(OauthConstant.CLIENT_ID.ADMIN)) {
            String userId = userService.getUserId(username, password);
            if(StringUtils.isNotEmpty(userId)) {
                return userId;
            }
        }
        return null;
    }

}
