package com.vincce.saas.dao;

import com.vincce.saas.domain.User;
import com.vincce.saas.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created By BaoNing On 2019/3/19
 */
@Component
public class UserDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * login
     * @param username
     * @param password
     * @return
     */
    public User checkUser(String username, String password){
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is(username).and("password").is(password));
        return mongoTemplate.findOne(query, User.class);
    }

    /**
     * add user
     * @param userVo
     * @return
     */
    @Transactional
    public boolean addUser(UserVo userVo){
        try {
            mongoTemplate.save(userVo);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    /**
     * get user
     * @param userVo
     * @return
     */
    public UserVo getUser(UserVo userVo){
        Query query = new Query();
        //通过手机号查询
        query.addCriteria(Criteria.where("phone").is(userVo.getPhone()));
        return mongoTemplate.findOne(query,UserVo.class);
    }

    /**
     * delete by id
     * @param id
     */
    @Transactional
    public void deleteById(String id){
        Query query = new Query(Criteria.where("id").is(id));
        mongoTemplate.remove(query,User.class);
    }

}
