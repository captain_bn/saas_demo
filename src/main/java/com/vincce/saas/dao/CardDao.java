package com.vincce.saas.dao;

import com.vincce.saas.common.base.APage;
import com.vincce.saas.domain.Card;
import com.vincce.saas.util.StringUtils;
import com.vincce.saas.vo.CardVo;
import com.vincce.saas.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/13
 */
@Component
public class CardDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private CardRepository cardRepository;


    /**
     * updateCard
     * @param cardVo
     * @return
     */
    public Card udpateCard(CardVo cardVo){
        String cardId = cardVo.getId();
        Query query = new Query(Criteria.where("id").is(cardId));
        Update update = new Update()
                .set("name",cardVo.getName())
                .currentDate("updateTime");
        Card card = mongoTemplate.findAndModify(query, update, Card.class);
        return card;
    }


    /**
     * add
     * @return
     */
    @Transactional
    public boolean addCard(Card card){
        try {
            mongoTemplate.save(card);
        }catch (Exception e){
            return false;
        }
        return true;
    }

    /**
     * get
     * @param name
     * @return
     */
    public Card getCard(String name){
        Query query = new Query(Criteria.where("name").is(name));
        Card card_mongo = mongoTemplate.findOne(query, Card.class);
        return card_mongo;
    }

    /**
     * all
     * @return
     */
    public List<Card> getAllCard(){
        return mongoTemplate.findAll(Card.class);
    }

    /**
     * update
     * @param name phone
     * @return
     */
    @Transactional
    public boolean updatePhoneByName(String name, String phone){
        Query query = new Query(Criteria.where("name").is(name));

/**
 * //////////////////////////////////////////////////////////////////////////////////
 */
//        //分页查询
//        Integer page = 0;
//        Integer size = 5;
//        PageRequest pageable = PageRequest.of(page,size);
//
//        Card card = new Card();
//        //构建匹配器
//        ExampleMatcher exampleMatcher = ExampleMatcher.matching();
//        //构建匹配器的匹配条件（想要模糊查询的字段，匹配规则）
//        exampleMatcher = exampleMatcher.withMatcher("name",ExampleMatcher.GenericPropertyMatchers.contains());
//        Example<Card> example = Example.of(card,exampleMatcher);
//        //根据分页以及条件查询
//        Page<Card> allPages = cardRepository.findAll(example,pageable);
//        //获得总页数
//        Integer totalPages = allPages.getTotalPages();

 /**
  * //////////////////////////////////////////////////////////////////////////////////
 */


        Update update = new Update().set("phone", phone)
                                    .currentDate("update_time")
                                    //.max("salary",10000)
                                    //.min("salary",9000)
                                    //.rename("age2","age")
                                    //.setOnInsert("age",40)
                                    //.unset("create_time22")
                                    .currentTimestamp("create_time");
        mongoTemplate.updateMulti(query, update, Card.class);
        return true;
    }

    /**
     * delete
     * @param name
     * @return
     */
    @Transactional
    public boolean deleteCard(String name){
        Query query = new Query(Criteria.where("name").is(name));
        mongoTemplate.remove(query, Card.class);
        return true;
    }


    /**
     * 测试另一种方式操作mongo
     * @param card
     * @return
     */
    @Transactional
    public void insert(Card card){
        mongoTemplate.insert(card);
    }


    /**
     * 同时插入多个数据
     * @param cardList
     * @return
     */
    @Transactional
    public void insertList(List<Card> cardList){
        mongoTemplate.insertAll(cardList);
    }

    /**
     * 查询并修改
     * @param cardVo
     */
    public void findAndUpdate(CardVo cardVo){
        Query query = new Query(Criteria.where("name").is(cardVo.getName()));
        Update update = new Update().set("phone",cardVo.getPhone());

        mongoTemplate.findAndModify(query,update,Card.class);
    }

    /**
     * 操作符查询
     * @return
     */
    public List<Card> queryByOperators(Integer paramter){
        //第一种写法
        //BasicQuery basicQuery = new BasicQuery("{age:{$lt:40}}");
        /**
         * lt:小于; lte:小于等于; gt:大于; gte:大于等于
         */
        //第二种写法
        Query query = new Query(Criteria.where("age").lt(paramter)); //.and()再可以添加
        return mongoTemplate.find(query, Card.class);
    }

    /**
     * 分页查询
     * @param pageVo
     * @return
     */
    public APage<Card> queryByPageable(PageVo pageVo){
        Integer pageIndex = pageVo.getPageIndex();
        Integer pageSize = pageVo.getPageSize();
        //默认按创建时间倒序
        Sort sort = Sort.by(Sort.Direction.DESC,"create_time");
        Pageable pageable = PageRequest.of(pageIndex -1, pageSize, sort);
        Query query = new Query();
        query.with(pageable);
        //统计总数
        long count = mongoTemplate.count(query, Card.class);
        //查询列表
        List<Card> cardList = mongoTemplate.find(query, Card.class);
        APage<Card> page = new APage<Card>();
        page.setList(cardList);
        page.setTotalCount((int) count);
        return page;
    }

//    /**
//     * query by example
//     * @param card
//     * @return
//     */
//    public Card queryByExample(Card card){
//        ExampleMatcher matcher = ExampleMatcher.matching();
//        Example<Card> example = Example.of(card, matcher);
//        Query query = new Query();
//        query.addCriteria(Criteria.byExample(example));
//        return mongoTemplate.findOne(query,Card.class);
//
//    }

    /**
     * 通过userId查询列表
     * @param userId
     * @return
     */
    public APage<Card> queryCardListByUserId(String userId){
        Sort sort = Sort.by(Sort.Direction.DESC,"create_time");
        APage page = new APage();
        Pageable pageable = PageRequest.of(page.getPageNo() - 1, page.getPageSize(), sort);
        Query query = new Query();
        query.with(pageable);
        if (StringUtils.isNotEmpty(userId)){
            query.addCriteria(Criteria.where("user_id").is(userId));
        }
        long total = mongoTemplate.count(query, Card.class);
        List<Card> productCollectList = mongoTemplate.find(query, Card.class);
        page.setTotalCount((int)total);
        page.setList(productCollectList);
        return page;
    }




}
