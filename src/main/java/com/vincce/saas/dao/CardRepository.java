package com.vincce.saas.dao;

import com.vincce.saas.domain.Card;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created By BaoNing On 2019/3/14
 */
@Repository
public interface CardRepository extends MongoRepository<Card,String> {

}
