package com.vincce.saas.dao;

import com.vincce.saas.domain.Book;
import com.vincce.saas.util.StringUtils;
import com.vincce.saas.vo.BookVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created By BaoNing On 2019/3/21
 */
@Component
public class ApiDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * save(保存一条记录)
     * @return
     */
    public void save(BookVo bookVo){
        mongoTemplate.save(bookVo);
    }

    /**
     * insertAll(插入多条记录)
     * @param bookList
     */
    public void insertAll(List<Book> bookList){
        mongoTemplate.insertAll(bookList);
    }

    /**
     * findById(根据id查询一条记录)
     * @param id
     * @return
     */
    public Book findById(String id){
        return mongoTemplate.findById(id, Book.class);
    }

    /**
     * findAll(查询所有记录)
     * @return
     */
    public List<Book> findAll(){
        return mongoTemplate.findAll(Book.class);
    }

    /**
     * upsert(先查询记录,并更新记录;若记录不存在,则新建一条记录)
     * @param book
     */
    public void upsert(Book book){
        Query query = new Query(Criteria.where("id").is(book.getId()));
        Update update = new Update().set("name",book.getName())
                                    .set("price",book.getPrice())
                                    .set("type",book.getType());
        mongoTemplate.upsert(query, update, Book.class);
    }

    /**
     * findAndModify(查询记录，并更新记录;若不存在,则不操作)
     * @param book
     */
    public Boolean findAndModify(Book book){
        Query query = new Query(Criteria.where("id").is(book.getId()));
        Update update = new Update().set("name",book.getName())
                                    .set("price",book.getPrice())
                                    .set("type",book.getType());
        book = mongoTemplate.findAndModify(query, update, Book.class);
        if (StringUtils.isNotEmpty(book)){
            return true;
        }
        return false;
    }

    /**
     * findAndRemove(先查询记录,若存在则删除记录;)
     * @param id
     * @return
     */
    public Book findAndRemove(String id){
        Query query = new Query(Criteria.where("id").is(id));
        return mongoTemplate.findAndRemove(query, Book.class);
    }

    /**
     * count
     * @param key 要统计的字段
     * @param value 要统计的字段值
     */
    public Long count(String key, Object value){
        Query query = new Query(Criteria.where(key).is(value));
        return mongoTemplate.count(query, Book.class);
    }

    /**
     * exists 判断是否存在
     * @param id
     * @return
     */
    public Boolean exists(String id){
        Query query = new Query(Criteria.where("id").is(id));
        return mongoTemplate.exists(query, Book.class);
    }





}
